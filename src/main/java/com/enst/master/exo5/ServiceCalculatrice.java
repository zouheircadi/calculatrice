package com.enst.master.exo5;


public class ServiceCalculatrice {

	protected Calculatrice creerCalculatrice() {
		return new CalculatriceImpl();
	}

	/**
	 * Calculate sum of the long parameters
	 * 
	 * @param val1 : fist parameter
	 * @param val2 : second parameter
	 * @return add val1 and val2
	 */
	public long additionner(long val1, long val2) {

		//create calculatrice
		//make calculation
		//return result
		
		//delete this fake return
		return 0;
	}
	
	/**
	 * Calculate difference between the two parameters 
	 * 
	 * @param val1 : fist parameter
	 * @param val2 : second parameter
	 * @return the difference between the two parameters
	 */
	public long soustraire(long val1, long val2) {

		//create calculatrice
		//make calculation
		//return result
		
		//delete this fake return
		return 0;
	}	
	
	/**
	 * 
	 * Calculate product of the two parameters 
	 * 
	 * @param val1 : fist parameter
	 * @param val2 : second parameter
	 * @return the product of the two parameters
	 */
	public long multiplier(long val1, long val2) {

		//create calculatrice
		//make calculation
		//return result
		
		//delete this fake return
		return 0;
	}	
	

	/**
	 * 
	 * Calculate division between the two parameters 
	 * 
	 * @param val1 : fist parameter
	 * @param val2 : second parameter
	 * @return division of val1 by val2
	 */
	public long diviser(long val1, long val2) {

		//create calculatrice
		//make calculation
		//return result
		
		//delete this fake return
		return 0;
	}	
	
	/**
	 * Calculate the sum of the two first parameters and
	 * substract the third parameter from the previous result
	 * 
	 * @param val1 : fist parameter
	 * @param val2 : second parameter
	 * @param val3 : third parameter
	 * @return the obtained result
	 */
	public long calculer(long val1, long val2, long val3) {

		//create calculatrice
		//make calculation add
		//make calculation substract on the temporary result
		//return result
		
		//delete this fake return
		return 0;
	}

}
